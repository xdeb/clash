## clash

A rule-based tunnel which provides:

  * Local HTTP/HTTPS/SOCKS server with authentication support.

  * VMess, Shadowsocks, Trojan. Snell protocol support for remote connections.

  * Built-in DNS server that aims to minimize DNS pollution attack impack,
    supportsDoH/DoT upstream and fake IP.

  * Rules based off domains, GeoIP, IP CIDR or ports to forward packets to
    different nodes.

  * Remote groups allow users to implement powerful rules. Supports automatic
    fallback, load balancing or auto select node based off latency.

  * Remote providers, allowing users to get node lists remotel instead of
    hardcoding in config.

  * Netfilter TCP redirecting. Deploy Clash on your Internet gateway with
    `iptables`.

  * Comprehensive HTTP RESTful API controller.

## Usage (APT installation)

1. Set up the APT key

   Add the repository's APT Key by following command:

        wget -O /etc/apt/trusted.gpg.d/xdeb-gitlab-io.gpg https://xdeb.gitlab.io/clash/pubkey.gpg

   or (if you prefer text formatted key file)

        wget -O /etc/apt/trusted.gpg.d/xdeb-gitlab-io.asc https://xdeb.gitlab.io/clash/pubkey.asc

2. Set up APT sources

   Create a new source list file at `/etc/apt/sources.list.d/xdeb-gitlab-io.list` with the
   content as following (debian/bullseye for example):

        deb https://xdeb.gitlab.io/clash/debian bullseye contrib

   The avaliable source list files are:

   * Debian 10 (buster)

         deb https://xdeb.gitlab.io/clash/debian buster contrib

   * Debian 11 (bullseye)

         deb https://xdeb.gitlab.io/clash/debian bullseye contrib

   * Ubuntu 18.04 TLS (Bionic Beaver)

         deb https://xdeb.gitlab.io/clash/ubuntu bionic universe

   * Ubuntu 20.04 TLS (Focal Fossa)

         deb https://xdeb.gitlab.io/clash/ubuntu focal universe

   * Ubuntu 22.04 TLS (Jammy Jellyfish)

         deb https://xdeb.gitlab.io/clash/ubuntu jammy universe

   * Ubuntu 22.10 (Kinetic Kudu)

         deb https://xdeb.gitlab.io/clash/ubuntu kinetic universe

3. Install `clash`

        apt-get update && apt-get install clash

## Usage (download binary)

All binaries are stored at `https://xdeb.gitlab.io/clash/clash_<OS>_<ARCH>.tar.gz`.

For example, you can download x64 binary for _GNU Libc_ at <https://xdeb.gitlab.io/clash/clash_gnu_amd64.tar.gz>. You can also download i386 binary for _MUSL Libc_ at <https://xdeb.gitlab.io/clash/clash_musl_i386.tar.gz>. Then unzip the downloaded file by `gzip -d` command. Change the file permission by `chmod` command finally.

For instance, download the binary on _Debian 11 (amd64)_, and install it, we can use a one-line command like,

    wget -O- https://xdeb.gitlab.io/clash/clash_gnu_amd64.tar.gz | tar -C /usr/local/bin -xz
