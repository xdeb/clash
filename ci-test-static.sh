#!/bin/bash

if [ "$HOSTNAME" = "${HOSTNAME#runner-}" ] # Don't print HOSTNAME on GitLab Runner.
then
    PS4='\n\[\033[1;93m\]${HOSTNAME}:${0}:${LINENO} + \e[m\]'
else
    PS4='\n\[\033[1;93m\]${0}:${LINENO} + \e[m\]'
fi

set -eux


# TEST: run binary file (x86_64)
./dist/static/amd64/clash -v | grep "^Clash $PKG_VERSION linux amd64 with go$GOVERSION "

# TEST: run binary file (x86)
./dist/static/386/clash -v | grep "^Clash $PKG_VERSION linux 386 with go$GOVERSION "

# TEST: install .apk package (x86_64)
apk add --allow-untrusted ./dist/clash_${PKG_VERSION}-${PKG_RELEASE}-xdeb_x86_64.apk ./test_data/clash-geoip_20230812-1-xdeb_all.apk
/usr/bin/clash -v | grep "^Clash $PKG_VERSION linux amd64 with go$GOVERSION "

# TEST: uninstall .apk package (x86_64)
apk del --no-network --rdepends clash
test ! -x /usr/bin/clash

# TEST: install .apk package (x86)
apk add --allow-untrusted ./dist/clash_${PKG_VERSION}-${PKG_RELEASE}-xdeb_x86.apk ./test_data/clash-geoip_20230812-1-xdeb_all.apk
/usr/bin/clash -v | grep "^Clash $PKG_VERSION linux 386 with go$GOVERSION "

# TEST: uninstall .apk package (x86_64)
apk del --no-network --rdepends clash
test ! -x /usr/bin/clash

# TEST: upgrade .apk package (x86_64)
apk add --allow-untrusted ./test_data/clash_1.17.0-1-xdeb_x86_64.apk ./test_data/clash-geoip_20230812-1-xdeb_all.apk
/usr/bin/clash -v | grep "^Clash 1.17.0 linux amd64 with go1.21.0 "
apk add --allow-untrusted ./dist/clash_${PKG_VERSION}-${PKG_RELEASE}-xdeb_x86_64.apk
/usr/bin/clash -v | grep "^Clash $PKG_VERSION linux amd64 with go$GOVERSION "
apk del clash

# TEST: upgrade .apk package (x86)
apk add --allow-untrusted ./test_data/clash_1.17.0-1-xdeb_x86.apk ./test_data/clash-geoip_20230812-1-xdeb_all.apk
/usr/bin/clash -v | grep "^Clash 1.17.0 linux 386 with go1.21.0 "
apk add --allow-untrusted ./dist/clash_${PKG_VERSION}-${PKG_RELEASE}-xdeb_x86.apk
/usr/bin/clash -v | grep "^Clash $PKG_VERSION linux 386 with go$GOVERSION "
apk del clash
