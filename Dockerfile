FROM alpine:latest
ADD ./dist/static/amd64/clash \
    ./Country.mmdb \
    /opt/clash/
ENTRYPOINT ["/opt/clash/clash", "-f", "/opt/clash/config.yaml", "-d", "/opt/clash"]
