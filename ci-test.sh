#!/bin/bash

if [ "$HOSTNAME" = "${HOSTNAME#runner-}" ] # Don't print HOSTNAME on GitLab Runner.
then
    PS4='\n\[\033[1;93m\]${HOSTNAME}:${0}:${LINENO} + \e[m\]'
else
    PS4='\n\[\033[1;93m\]${0}:${LINENO} + \e[m\]'
fi

set -eux


##########################
##                      ##
##  SET UP TESTING ENV  ##
##                      ##
##########################

. /etc/os-release
case $ID in
debian|ubuntu)
    case $VERSION_CODENAME in
    buster|bullseye|bookworm|focal|jammy|lunar|mantic)
        CODENAME=$VERSION_CODENAME
        ;;
    *)
        echo "ERROR: VERSION_CODENAME not found: $VERSION_CODENAME"
        exit 1
        ;;
    esac
    ;;
*)
    echo "ERROR: ID not found: $ID"
    exit 1
    ;;
esac


################
##            ##
##  DO TESTS  ##
##            ##
################


# TEST: dynamic linked binary
case $CODENAME in
buster|bullseye|focal)
    ./dist/bullseye/amd64/clash -v | grep "^Clash $PKG_VERSION linux amd64 with go$GOVERSION "
    ;;
bookworm|jammy|lunar|mantic)
    ./dist/bookworm/amd64/clash -v | grep "^Clash $PKG_VERSION linux amd64 with go$GOVERSION "
    ;;
*)
    echo "ERROR: CODENAME not found: $CODENAME"
    exit 1
    ;;
esac
./dist/bullseye/386/clash -v | grep "^Clash $PKG_VERSION linux 386 with go$GOVERSION "
./dist/bookworm/386/clash -v | grep "^Clash $PKG_VERSION linux 386 with go$GOVERSION "

# TEST: static linked binary
./dist/static/amd64/clash -v | grep "^Clash $PKG_VERSION linux amd64 with go$GOVERSION "
./dist/static/386/clash -v | grep "^Clash $PKG_VERSION linux 386 with go$GOVERSION "

# TEST: install .deb package (dynamic)
apt-get install -y ./dist/clash_${PKG_VERSION}-${PKG_RELEASE}-xdeb~${CODENAME}_amd64.deb ./test_data/clash-geoip_20230812-1-xdeb_all.deb
/usr/bin/clash -v | grep "^Clash $PKG_VERSION linux amd64 with go$GOVERSION "

# TEST: uninstall .deb package (dynamic)
apt-get autoremove --purge -y clash
test ! -x /usr/bin/clash

# TEST: upgrade .deb package (dynamic)
apt-get install -y ./test_data/clash_1.17.0-1-xdeb~${CODENAME}_amd64.deb ./test_data/clash-geoip_20230812-1-xdeb_all.deb
/usr/bin/clash -v |grep "^Clash 1.17.0 linux amd64 with go1.21.0 "
apt-get install -y ./dist/clash_${PKG_VERSION}-${PKG_RELEASE}-xdeb~${CODENAME}_amd64.deb
/usr/bin/clash -v | grep "^Clash $PKG_VERSION linux amd64 with go$GOVERSION "

# TEST: install .deb package (static)
apt-get install -y ./dist/clash-static_${PKG_VERSION}-${PKG_RELEASE}-xdeb_amd64.deb ./test_data/clash-geoip_20230812-1-xdeb_all.deb
/usr/bin/clash -v | grep "^Clash $PKG_VERSION linux amd64 with go$GOVERSION "

# TEST: uninstall .deb package (static)
apt-get autoremove --purge -y clash-static
test ! -x /usr/bin/clash

# TEST: upgrade .deb package (static)
apt-get install -y ./test_data/clash-static_1.17.0-1-xdeb_amd64.deb ./test_data/clash-geoip_20230812-1-xdeb_all.deb
/usr/bin/clash -v |grep "^Clash 1.17.0 linux amd64 with go1.21.0 "
apt-get install -y ./dist/clash-static_${PKG_VERSION}-${PKG_RELEASE}-xdeb_amd64.deb
/usr/bin/clash -v | grep "^Clash $PKG_VERSION linux amd64 with go$GOVERSION "
